"""
This module exposes some constants, which of course might be better suited to get into other
modules later on
"""

from os import environ, path

APP_NAME = 'ssh-sync'

VERSION = '0.0.1'

CONFIG_HOME = environ.get(
    'XDG_CONFIG_HOME',
    path.join(environ['HOME'], '.config', APP_NAME),
)
CONFIG_FILE = path.join(CONFIG_HOME, APP_NAME)

BANNER = """
%s v%s
""" % (APP_NAME, VERSION)
