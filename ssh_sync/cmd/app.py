"""The Applications module"""

import argparse
from sys import argv
from typing import List, Optional

from . import init_controller
from ..consts import BANNER


class App:  # pylint: disable=too-few-public-methods
    """The Application class"""

    __parser: argparse.ArgumentParser = argparse.ArgumentParser(prog=argv[0])
    __args: Optional[argparse.Namespace] = None

    def __init__(self) -> None:
        self.__parser.add_argument(
            '--version', action='version', version=BANNER)
        self.__parser.add_argument(
            '-v',
            '--verbose',
            dest='verbose',
            help='makes the output verbose',
            action='store_true')
        self.__parser.add_argument(
            '-p',
            '--pretend',
            dest='pretend',
            help='performs a dry run',
            action='store_true')

        subparsers = self.__parser.add_subparsers()
        init_controller.sub_parser(subparsers)

    def run(self, args: Optional[List[str]] = None):
        """Runs the application"""
        self.__args = self.__parser.parse_args(args)


def main(args: Optional[List[str]] = None):
    """entry point"""
    app: App = App()
    app.run(args)


if __name__ == "__main__":
    main()
