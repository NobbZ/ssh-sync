"""The init subcommand"""

import argparse


def sub_parser(subs) -> None:
    """Adds the init subparser"""
    parser: argparse.ArgumentParser = subs.add_parser(
        'init', help='initializes ssh-sync')
    parser.add_argument(
        '-b',
        '--bits',
        dest='bits',
        help='specifies keylength',
        type=int,
        action='store')
