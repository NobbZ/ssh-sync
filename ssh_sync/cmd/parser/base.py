# pylint: disable=missing-docstring

from abc import ABC, abstractmethod
from typing import List, Optional, Union
from argparse import ArgumentParser
from sys import argv

ArgV = List[str]  # pylint: disable=invalid-name


class BaseApp(ABC):
    __args: Optional[ArgV]
    __parsed = None
    __parser: ArgumentParser

    def __init__(self: 'BaseApp', args: Optional[ArgV] = None) -> None:
        self.__args = args
        self.__parser = ArgumentParser(argv[0])
        for arg in self.arguments:
            flags, meta = arg
            self.__parser.add_argument(*flags, **meta)

    def __enter__(self: 'BaseApp') -> 'BaseApp':
        self.__parse()
        return self

    def __exit__(self: 'BaseApp', *args) -> None:
        pass

    def __getattr__(self: 'BaseApp', key: str) -> Union[bool, int, str]:
        if self.__parsed is None or self.__parsed[key] is None:
            raise AttributeError
        else:
            return self.__parsed[key]

    def __parse(self: 'BaseApp') -> None:
        self.__parsed = vars(self.__parser.parse_args(self.__args))

    @property
    @abstractmethod
    def arguments(self) -> List:
        pass
