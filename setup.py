# pylint: disable=missing-docstring

from setuptools import setup, find_packages

from ssh_sync.consts import VERSION

setup(
    name='ssh-sync',
    version=VERSION,
    description='',
    packages=find_packages(),
    entry_points={'console_scripts': ['ssh-sync = ssh_sync.cmd.app:main']},
    author='Norbert Melzer',
    author_email='timmelzer@gmail.com',
    install_requires=[],
)
