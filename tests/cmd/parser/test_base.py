import unittest as ut

from ssh_sync.cmd.parser.base import BaseApp


class StubApp(BaseApp):
    arguments = [(['-v', '--verbose'], dict(
        dest='verbose', action='store_true'))]


class TestBaseApp(ut.TestCase):
    def test_parse_verbose(self):
        with StubApp(['--verbose']) as app:
            self.assertTrue(app.verbose)

    def test_no_verbose(self):
        with StubApp([]) as app:
            self.assertFalse(app.verbose)