ARTIFACT_FOLDERS=.mypy_cache build dist ssh_sync.egg-info $(shell find . -not \( -path './env' -prune \) -type d -name '__pycache__')

SRC_FILES=$(shell find ssh_sync -name '*.py')
TST_FILES=$(shell find tests -name '*.py')
PY_FILES=${SRC_FILES} ${TST_FILES}

run:
	PYTHONPATH=$(shell pwd) python bin/ssh-sync

test:
	pytest

lint: ${SRC_FILES:%=%_lint}

format: ${PY_FILES:%=%_isort} ${PY_FILES:%=%_format}

mypy: ${PY_FILES:%=%_mypy}

check: format lint mypy

clean: ${ARTIFACT_FOLDERS:%=%_clean_rf}

venv: env_clean_rf
	virtualenv --python=python3 env
	. env/bin/activate && \
		pip install pytest mypy pylint yapf isort && \
		python setup.py egg_info && \
		if [ -f ssh_sync.egg-info/requires.txt ]; then \
			pip install -r ssh_sync.egg-info/requires.txt; \
		fi

%_clean_rf:
	rm -rf $*

%_isort:
	isort $*

%_format:
	yapf --style=format.ini --in-place $*

%_lint:
	pylint $*

%_mypy:
	mypy $*
